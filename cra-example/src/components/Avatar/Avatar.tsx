import React from "react";
import "./Avatar.css";

interface Props {
  url: string;
  name: string;
}

const Avatar: React.FC<Props> = ({ url , name  }) => {
  return <img src={url} alt={name} />;
};

export default Avatar;
