import React from 'react';
import './List.css'
import {lis} from '../types'
interface Props{
    dd:lis;
}
const List:React.FC<Props> = ({dd}) => {
  return (
  <div className="listItem">
      <h3>{dd.heading}</h3>
      {
        dd.item.map((item)=>(
         <p>{item}</p>
        ))
      }
  </div>
  );
}

export default List;
