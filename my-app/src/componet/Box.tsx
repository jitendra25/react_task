import React from 'react';
import './Box.css'
import {det} from '../types'
interface Props{
  dd : det;
}
interface State{
   check : boolean;
}
class Box extends React.Component<Props,State>  {
    constructor(props:Props){
      super(props);
     this.state ={
       check : props.dd.active
     };
    }
    
 toggle = () =>{
    
  this.setState((state,props)=>({
    check:!state.check
  }));

 }
 
  render() {
     
  const btnStyle = {
    border :this.state.check ? "" : "2px blue solid",
    backgroundColor : this.state.check ? "blue" : "",
    color : this.state.check ?"white":"blue",
    boxShadow : this.state.check?"0px 1px 4px #7A7A7A":""
  }
 const divStyle ={
   marginTop :this.state.check?"30px":"60px"
 }
    console.log("rendered")
    return(
   <div className="content" style={divStyle} onClick={this.toggle}>
     <div className="plan" >
       <p>{this.props.dd.heading}</p>
     {
       this.state.check && (
         <p className="title">{this.props.dd.title}</p>
       )
     }
     </div>
     <div className="price">
      <p> <span>{this.props.dd.price}</span>/mo</p>
     </div>
     <div className= "feature">
       {
         this.props.dd.feature.map((value)=>(
           <p>{value}</p>
         ))
       }
     </div>
     <div className="btn">
       <a href="https://jksuthar12.github.io/portfolio1/" style={btnStyle}>{this.props.dd.button}</a>
     </div>
   </div>
  );
      }
}

export default Box;
