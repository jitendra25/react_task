import React from 'react';
import './App.css';
import {det , lis} from './types'
import Box from './componet/Box'
import List from './componet/List';
const detail:det[] = [{ 
    heading : "Free",
    price : "$0",
    button : "SIGN UP FOR FREE",
    active : false,
       title:"It's free",
    feature : ["10 users included" , "2 GB of storage","Help center access","Email support"]
  },
  {
       heading: "Pro",
       price :"$15",
       button : "GET STARTED",
          active : true,
          title:"Most popular",
       feature :["20 users included ","10 GB of storage","Help center access","Priority email support"]
  },
  {
    heading:"Enterprise",
    price: "$30",
    button : "CONTACT US",
       active : false,
          title:"Best for you",
    feature:["50 users included","30 GB of storage","Help center access","Phone & email support"]
  }
] ;
const list : lis[] = [
  {heading : "Company",
   item : ["Team","History","Contact us","Locatios"]
  },
   {
     heading : "Features",
     item : ["Cool stuff","Random feature","Team feature","Developer stuff","Another one"]

   },
   {
     heading : "Resources",
     item : ["Resource","Resource name","Another resource","Final resource"]
   },
   {
     heading : "Legal",
     item :["Priacy policy","Terms of use"]
   }

]
const App:React.FC =() => {
  return (
    <div >
      <p className={"heading"}>Pricing</p>
      <p className={"text"}>
        Quickly build an effective pricing table for your
        potential customers with this layout. it's built with default 
        Material-UI components with litter customization.
      </p>
      <div className="container">
     {
       detail.map((c,index)=>(
          <Box  dd={c} key={index}/>
  ))
     }
      </div>
     <hr/>
     <div className="listContainer">
       {
         list.map((value,index)=>(
           <List dd={value} key={index}/>
         ))
       }
     </div>
    </div>
  );
}

export default App;
