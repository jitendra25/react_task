import React from "react";
import { Customer } from "../../types";
import "./ListItem.css";
import Avatar from "../Avatar/Avatar";

interface Props {
  customer: Customer;
}

const ListItem: React.FC<Props> = ({ customer }) => {
  const check = {
    opacity : customer.active ? 0.5 : 1
  }
  return (
    <li style={check} >
      <div className={"avatar"}>
        <Avatar name={customer.name} url={customer.profileImage} />
      </div>
      <div className={"content"}>
        <div className={"title"}>{customer.name}</div>
        <div className={"subtitle"}>
          <div>{customer.email}</div>
          <div>{customer.phone}</div>
        </div>
      
      </div>
        <div>
          { customer.active && (
           <span className={"active"}>INACTIVE</span>
          )
          }
        </div>
    </li>
  );
};

export default ListItem;
